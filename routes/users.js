var express = require('express');
var router = express.Router();

/* Say hello to user. */
router.get('/', function(req, res, next) {
  res.send('Hello ', req.params.user);
});

module.exports = router;
